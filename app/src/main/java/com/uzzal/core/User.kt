package com.uzzal.core

import com.google.gson.annotations.SerializedName

/**
 * Created by Mahabubul Hasan <codehasan@gmail.com> on 12/9/2017.
 */
data class User(
        var name:String,
        var username:String,
        @SerializedName("email_address") var email:String,
        var phone:String)