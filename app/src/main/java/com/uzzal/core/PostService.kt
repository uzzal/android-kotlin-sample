package com.uzzal.core

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Mahabubul Hasan <codehasan@gmail.com> on 12/9/2017.
 */
interface PostService {

    @POST("/post.php")
    fun submitData(@Body user: User): Call<PostResponse>
}