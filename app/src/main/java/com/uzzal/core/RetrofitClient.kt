package com.uzzal.core

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Mahabubul Hasan <codehasan@gmail.com> on 12/9/2017.
 */
object RetrofitClient {
    fun get():Retrofit{
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2")
                .build()
    }

    fun postService():PostService{
        return get().create(PostService::class.java)
    }

    fun getService():GetService{
        return get().create(GetService::class.java)
    }
}