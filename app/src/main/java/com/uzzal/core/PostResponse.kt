package com.uzzal.core

/**
 * Created by Mahabubul Hasan <codehasan@gmail.com> on 12/9/2017.
 */
data class PostResponse(val status:String)