package com.uzzal.core

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Mahabubul Hasan <codehasan@gmail.com> on 12/9/2017.
 */
interface GetService {
    @GET("/get.php")
    fun getUser():Call<List<User>>
}