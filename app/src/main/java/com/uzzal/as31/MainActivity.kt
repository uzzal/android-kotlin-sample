package com.uzzal.as31

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.uzzal.core.PostResponse
import com.uzzal.core.RetrofitClient
import com.uzzal.core.User

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        txtMsg.text = "This is just amazing!"
        btnPost.setOnClickListener({
            println("hit for post")
            postPayload()
        })

        fab.setOnClickListener { view ->

            Handler().post {
                println("inside thread")
                payload()
            }

            Snackbar.make(view, "Payload sent!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun postPayload(){
        val user = User("uzzal", "u22al", "uzzal@example.com", "phone")
        val call:Call<PostResponse> = RetrofitClient.postService().submitData(user)
        call.enqueue(object :Callback<PostResponse>{
            override fun onResponse(call: Call<PostResponse>?, response: Response<PostResponse>?) {
                println("successfully post")
            }

            override fun onFailure(call: Call<PostResponse>?, t: Throwable?) {
                println("failure to post")
            }
        })
    }

    private fun payload(){
        val call:Call<List<User>> = RetrofitClient.getService().getUser()
        call.enqueue(object :Callback<List<User>>{
            override fun onResponse(call: Call<List<User>>?, response: Response<List<User>>?) {
                println("success")
                val list: List<User>? = response?.body()
                list?.withIndex()?.forEach { (index, user) ->
                    run {
                        println("name: ${user.name}")
                        println("email: ${user.email}")
                    }
                }
            }

            override fun onFailure(call: Call<List<User>>?, t: Throwable?) {
                println("failure")
                println(t?.message)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
